const init = require('./init')
const restaurantService = require('./restaurant-service')
const orderService = require('./order-service')
const userService = require('./user-service')

module.exports = {
  init,
  restaurantService,
  orderService,
  userService,
}
