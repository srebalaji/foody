const { Restaurant, RestaurantRating } = require('../models/')

const calculateRating = (ratings) => {
  if (ratings.length > 0) {
    const sum = ratings.reduce((a, b) => parseInt(a, 10) + parseInt(b, 10))
    return sum / ratings.length
  }
  return 0
}

const list = (db) => db.restaurants.map((restaurant) => {
  const restaurantRatings = db.restaurantsRatings[restaurant.id]
  restaurant.ratings = calculateRating(restaurantRatings)
  return restaurant
})

const search = (db, query) => db.restaurants
  .filter((restaurant) => restaurant.name.toLowerCase() === query.toLowerCase())

const review = (db, restaurantId, rating, user) => {
  const restaurant = Restaurant.getRestaurant(db, restaurantId)
  if (restaurant) {
    const newRating = new RestaurantRating(restaurantId, rating, user.id)
    db.restaurantsRatings[restaurantId].push(newRating.rating)
    return {
      restaurant: {
        id: restaurant.id,
        name: restaurant.name,
        rating: newRating.rating,
        user: newRating.userId,
      },
    }
  }
  return {
    msg: 'No user or restaurant',
  }
}

module.exports = {
  list,
  search,
  review,
}
