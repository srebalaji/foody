const { User, Dish, Order } = require('../models')

const getItemsFromCart = (db, user) => {
  const { cart } = user
  if (cart.length > 0) {
    return cart.map((cartItem) => Dish.getById(db, cartItem))
  }
  return []
}

const addToCart = (db, userId, items) => {
  const user = User.getById(db, userId)
  user.addToCart(db, items)
  return user
}

const createOrder = (db, userId, address, paymentMode) => {
  const user = User.getById(db, userId)
  const items = getItemsFromCart(db, user)
  const order = new Order(undefined, userId, items, address, paymentMode, db)

  if (order.isDeliveryChargeApplicable()) {
    order.deliveryCharge = Order.deliveryAmount()
  }

  user.clearCart(db)
  return order
}

module.exports = {
  addToCart,
  createOrder,
}
