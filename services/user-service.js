const { User } = require('../models')

const register = (db, name, password) => {
  const user = new User(undefined, name, password)
  User.register(db, user)
  return user
}

const login = (db, name, password) => {
  const user = User.getByName(db, name)
  if (user) {
    if (user.authenticate(password)) {
      return user
    }
    return {
      msg: 'Authentication failed.',
    }
  }
  return {
    msg: 'No user found',
  }
}

const getUserByToken = (db, token) => {
  const user = User.getByToken(db, token)
  if (user) return user
  return null
}

const update = (db, user, paymentMode) => {
  user.preferredPaymentMode = paymentMode
  db.users.filter((dbUser) => dbUser.id === user.id)[0] = user
  return user
}

module.exports = {
  register,
  login,
  getUserByToken,
  update,
}
