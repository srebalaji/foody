const {
  Restaurant,
  Dish,
  User,
} = require('../models')

const createUser = (id, name) => new User(id, name)

const createDish = (name, restaurantId) => new Dish(undefined, name, restaurantId, Math.round(Math.random(10, 100) * 100))

const creteDishes = (dishes, restaurantId) => dishes
  .map((dish) => createDish(dish, restaurantId))

const createRestaurant = (id, name, foodType, dishes) => {
  const restaurant = new Restaurant(id, name, foodType, dishes)
  restaurant.dishes = creteDishes(dishes, restaurant.id)
  return restaurant
}

const createModels = () => {
  const restaurants = []
  const dishes = []
  const users = []
  const orders = {}
  const restaurantsRatings = {}

  const Taj = createRestaurant('1', 'Taj', 'North Indian', ['fruit salad', 'dosa'])
  const Saravana = createRestaurant('2', 'Saravana', 'South Indian', ['Chappati', 'Vada', 'Dosa'])
  const Subway = createRestaurant('3', 'Subway', 'Fast Food', ['Sub', 'Roll'])

  restaurants.push(Taj)
  restaurants.push(Saravana)
  restaurants.push(Subway)
  dishes.push(...Taj.dishes)
  dishes.push(...Saravana.dishes)
  dishes.push(...Subway.dishes)

  const user1 = createUser('1', 'User1')
  const user2 = createUser('2', 'User2')
  users.push(user1)
  users.push(user2)

  orders[user1.id] = []
  orders[user2.id] = []

  restaurantsRatings[Taj.id] = []
  restaurantsRatings[Saravana.id] = []
  restaurantsRatings[Subway.id] = []

  return {
    restaurants,
    dishes,
    users,
    restaurantsRatings,
    orders,
  }
}


const init = () => {
  console.log('Creating DB')
  return createModels()
}

module.exports = init
