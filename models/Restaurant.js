const uniqid = require('uniqid')

class Restaurant {
  constructor(id = uniqid(), name, type, dishes = []) {
    this.id = id
    this.name = name
    this.type = type
    this.dishes = dishes
  }

  addDish(newDish) {
    this.dishes.push(newDish)
  }

  static getRestaurant(db, id) {
    return db.restaurants.filter((restaurant) => restaurant.id === id)[0]
  }
}

module.exports = Restaurant
