const Restaurant = require('./Restaurant')
const Dish = require('./Dish')
const User = require('./User')
const RestaurantRating = require('./RestaurantRating')
const Order = require('./Order')

module.exports = {
  Restaurant,
  Dish,
  User,
  RestaurantRating,
  Order,
}
