const uniqid = require('uniqid')

class Dish {
  constructor(id = uniqid(), name, restaurantId, price) {
    this.id = id
    this.name = name
    this.restaurantId = restaurantId
    this.price = price
  }

  static getById(db, id) {
    return db.dishes.filter((dish) => dish.id === id)[0]
  }
}

module.exports = Dish
