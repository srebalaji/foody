const uniqid = require('uniqid')

class RestaurantRating {
  constructor(restaurantId, rating, userId) {
    this.id = uniqid()
    this.restaurantId = restaurantId
    this.rating = rating
    this.userId = userId
  }
}

module.exports = RestaurantRating
