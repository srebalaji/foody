const uniqid = require('uniqid')

class User {
  constructor(id = uniqid(), name, password) {
    this.id = id
    this.name = name
    this.password = password
    this.cart = []
    this.accessToken = uniqid.time()
    this.preferredPaymentMode = undefined
  }

  addToCart(db, items) {
    this.cart.push(...items)
    this.cart = [...new Set(this.cart)]
    db.users.filter((user) => user.id === this.id)[0].cart = this.cart
  }

  clearCart(db) {
    db.users.filter((user) => user.id === this.id)[0].cart = []
  }

  authenticate(password) {
    if (this.password === password) return true
    return false
  }

  static getById(db, id) {
    return db.users.filter((user) => user.id === id)[0]
  }

  static getByName(db, name) {
    return db.users.filter((user) => user.name === name)[0]
  }

  static getByToken(db, token) {
    return db.users.filter((user) => user.accessToken === token)[0]
  }

  static register(db, userObj) {
    return db.users.push(userObj)
  }
}

module.exports = User
