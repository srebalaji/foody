const uniqid = require('uniqid')

const thresholdAmount = 99

const calcAmount = (items) => items
  .map((item) => item.price)
  .reduce((a, b) => a + b)

class Order {
  constructor(
    id = uniqid(),
    userId,
    items,
    deliveryAddress,
    paymentMode,
    db,
  ) {
    this.id = id
    this.userId = userId
    this.items = items
    this.paymentMode = paymentMode
    this.deliveryAddress = deliveryAddress
    this.deliveryCharge = 0
    db.orders[this.userId].push(this)
  }

  // Checks if delivery charge is applicable
  isDeliveryChargeApplicable() {
    if (calcAmount(this.items) < thresholdAmount) return true
    return false
  }

  // Return total amount
  totalAmount() {
    return calcAmount(this.items) + this.deliveryCharge
  }

  static deliveryAmount() {
    return 200
  }

  static paymentMethods() {
    return {
      cod: 'Cash On Delivery',
      cc: 'Credit Card',
      nb: 'Net Banking',
    }
  }
}

module.exports = Order
