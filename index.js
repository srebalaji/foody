const express = require('express')
const bodyParser = require('body-parser')

const {
  init,
  restaurantService,
  orderService,
  userService,
} = require('./services/')

const app = express()
app.use(bodyParser.urlencoded())
app.use(bodyParser.json())

const db = init()

app.get('/', (req, res) => {
  if (req.query && req.query.q) {
    res.json({ restaurants: restaurantService.search(db, req.query.q) })
  } else {
    res.json({ restaurants: restaurantService.list(db) })
  }
})

app.post('/review/:id', (req, res) => {
  const accessToken = req.header('access-token')
  const user = userService.getUserByToken(db, accessToken)
  if (user) {
    const {
      rating,
    } = req.body

    const {
      id,
    } = req.params

    const result = restaurantService.review(db, id, rating, user)
    res.json(result)
  }
  res.json({
    msg: 'Not auhenticated',
  })
})

app.post('/user/cart', (req, res) => {
  const accessToken = req.header('access-token')
  const user = userService.getUserByToken(db, accessToken)
  if (user) {
    const {
      dishIds,
    } = req.body
    res.json(orderService.addToCart(db, user.id, dishIds))
  }
  res.json({
    msg: 'Not auhenticated',
  })
})

app.post('/user/order', (req, res) => {
  const accessToken = req.header('access-token')
  const user = userService.getUserByToken(db, accessToken)
  if (user) {
    const {
      deliveryAddress,
      paymentMode,
    } = req.body

    res.json(orderService.createOrder(db, user.id, deliveryAddress, paymentMode))
  }
  res.json({
    msg: 'Not auhenticated',
  })
})

app.post('/user/register', (req, res) => {
  const { name, password } = req.body
  res.json(userService.register(db, name, password))
})

app.post('/user/login', (req, res) => {
  const { name, password } = req.body
  res.json(userService.login(db, name, password))
})


app.put('/user', (req, res) => {
  const accessToken = req.header('access-token')
  const user = userService.getUserByToken(db, accessToken)
  if (user) {
    const {
      paymentMode,
    } = req.body
    res.json(userService.update(db, user, paymentMode))
  }
  res.json({
    msg: 'Not auhenticated',
  })
})
app.get('/test', (req, res) => {
  res.json(db)
})

app.listen(3000)
